import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableHighlight } from 'react-native';
import { Actions } from 'react-native-router-flux';

class Container extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <View style={{ display: 'flex', flexDirection: 'column', height: '100%' }}>
                <View style={style.header}>
                    <Image
                        source={require('../karate/img/header.png')}
                    />
                </View>
                <View style={style.content}>
                    <ImageBackground
                        source={require('../karate/img/background.jpg')}
                        style={{
                            display: 'flex',
                            flex: 1,
                            width: '100%',
                            height: '100%',
                            flexDirection: 'column',
                            // justifyContent: 'center',
                        }}
                    >
                        {this.props.breadcumb &&
                            <View style={style.menu}>
                                <View style={{ flex: 1 }}>
                                    <Text style={style.menuTxt}>{this.props.judul}</Text>
                                </View>
                                <View style={{ flex: 0.5 }}>
                                    <TouchableHighlight onPress={() => Actions.push('home')}>
                                        <Text style={style.menuTxtRight}>{"Home"}</Text>
                                    </TouchableHighlight>
                                </View>
                            </View>
                        }
                        {this.props.children}
                    </ImageBackground>
                </View>
            </View >
        )
    }
}

export default Container

const style = {
    header: {
        // flex: 1,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    content: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    menu: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: 'rgba(0,0,0,0.5)',
        marginBottom: 15,
    },
    menuTxt: {
        color: "#fff",
        fontSize: 18,
        paddingBottom: 15,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
    },
    menuTxtRight: {
        color: "#fff",
        fontSize: 18,
        textAlign: 'right',
        paddingBottom: 15,
        paddingTop: 15,
        paddingLeft: 15,
        paddingRight: 15,
    }
}