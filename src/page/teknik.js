import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native'
import Container from '../component/container';
import { Actions } from 'react-native-router-flux';

const img = require('../karate/img/image-splash-screen.jpg');

class Teknik extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        console.log("AAAAAAAAAAAA")
        return (
            <Container breadcumb judul="Teknik">
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Teknik Kata", data: data.kata })}>
                    <Text style={style.liTxt}>Teknik Kata</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.kihon()}>
                    <Text style={style.liTxt}>Teknik Kihon</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Teknik Kumite", data: data.kumite })}>
                    <Text style={style.liTxt}>Teknik Kumite</Text>
                </TouchableOpacity>
            </Container >
        )
    }
}

export default Teknik

const style = {
    li: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom: 15,
        marginLeft: 50,
        marginRight: 50,
        borderWidth: 3,
    },
    liTxt: {
        fontSize: 22,
        fontWeight: 'bold',
    }
}

const data = {
    kumite: `
        <h3>
            Istilah Kumite (Bertarung)
        </h3>
        <div>
            1. KIHON IPPON KUMITE : Pertarungan Dasar Satu Langkah.
        </div>
        <div>
            2. GO-HON KUMITE : Pertarungan Lima Langkah.
        </div>
        <div>
            3. SANBON KUMITE : Pertarungan Tiga Langkah.
        </div>
        <div>
            4. KEASHI IPPON KUMITE : Pertarungan Dua Langkah.
        </div>
        <div>
            5. JIYU IPPON KUMITE : Pertarungan Semi Bebas.
        </div>
        <div>
            6. OKURI JIYU IPPON KUMITE : Pertarungan Semi Bebas Dua Langkah.
        </div>
        <div>
            7. JIYU KUMITE : Pertarungan Gaya Bebas.
        </div>
        <div>
            <br/>
        </div>`,
    kata: `
        <div>
            <h3>Heidan Shodan : Pikiran yang damai (1)</h3>
            <img src="http://www.kanseikaratedoshotokan.com/blog/wp-content/uploads/2014/12/heian-shodan.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Heian Nidan : Pikiran yang damai (2)</h3>
            <img src="https://4.bp.blogspot.com/-x97UhgyF7cM/WKVmGgq8hXI/AAAAAAAACGM/HVNH2mAZaWEOTBkVkSRhSyGGtyItxORzQCLcB/s1600/kata%2Bkarate.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Heian Sandan :  Pikiran yang damai (3)</h3>
            <img src="http://www.kanseikaratedoshotokan.com/blog/wp-content/uploads/2014/12/heian-sandan.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Heidan Yondan : Pikiran yang damai (4)</h3>
            <img src="http://cdn.simplesite.com/i/03/d5/285415630756566275/i285415639389012930._szw1280h1280_.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Heian Godan : Pikiran yang damai (5)</h3>
            <img src="http://www.kanseikaratedoshotokan.com/blog/wp-content/uploads/2014/12/heian-godan.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Tekki Shodan : Satria yang kuat, kuda-kuda yang kuat (1)</h3>
            <img src="http://www.kanseikaratedoshotokan.com/blog/wp-content/uploads/2014/12/tekki-shodan.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Tekki Nidan : Satria yang kuat, kuda-kuda yang kuat (2)</h3>
            <img src="http://www.karatekas.com/katas/tekki2/tekkinidana.gif" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Tekki Sandan : Satria yang kuat, kuda-kuda yang kuat (3)</h3>
            <img src="http://www.brevardshotokan.com/static.2/images/kata/tekki-sandan.png" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
        <div>
            <h3>Bassai Dai : Menembus benteng (besar)</h3>
            <img src="https://4.bp.blogspot.com/-2s3NRU9PKrk/V4yU9Z1yZMI/AAAAAAAAA4U/ldCIWT6FAJoPZtjMO_ZcM1KoqbI5KQWnQCLcB/s1600/bassai_dai.jpg" />
            <div>
                <div>
                    <a href="#">Download</a>
                </di>
                <div>
                    <a href="#">Watch</a>
                </di>
            </div>
        </div>
    `,
}