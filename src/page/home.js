import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Container from '../component/container';

const style = {
    wrapperMenu: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageMenu: {
        flex: 1,
        width: 100,
        height: 100,
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    titleMenu: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 24,
        color: "#fff"
    }
}

class Home extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Container>
                <View style={style.wrapperMenu}>
                    <TouchableOpacity onPress={() => Actions.tentang()}>
                        <View style={{ width: 100, height: 100, }}>
                            <Image
                                source={require('../karate/img/menu-tentang.png')}
                                resizeMode="cover"
                                style={style.imageMenu}

                            />
                        </View>
                        <Text style={style.titleMenu}>Tentang</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ display: 'flex', flex: 1, flexDirection: 'row' }}>
                    <View style={style.wrapperMenu}>
                        <TouchableOpacity onPress={() => Actions.teknik()}>
                            <View style={{ width: 100, height: 100, }}>
                                <Image
                                    source={require('../karate/img/menu-teknik.jpg')}
                                    resizeMode="cover"
                                    style={style.imageMenu}

                                />
                            </View>
                            <Text style={style.titleMenu}>Teknik</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={style.wrapperMenu}>
                        <TouchableOpacity onPress={() => Actions.latihan()}>
                            <View style={{ width: 100, height: 100, }}>
                                <Image
                                    source={require('../karate/img/menu-latihan.png')}
                                    resizeMode="cover"
                                    style={style.imageMenu}

                                />
                            </View>
                            <Text style={style.titleMenu}>Latihan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Container>
        )
    }
}

export default Home