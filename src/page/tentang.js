import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native'
import Container from '../component/container';
import { Actions } from 'react-native-router-flux';

class Tentang extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Container breadcumb judul="Tentang">

                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Filosofi", data: data.filosofi })}>
                    <Text style={style.liTxt}>Filosofi Karate</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Istilah", data: data.istilahKarate })}>
                    <Text style={style.liTxt}>Istilah</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Sejarah Karate", data: data.sejarahKarate })}>
                    <Text style={style.liTxt}>Sejarah Karate</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Teknik Karate", data: data.teknikKarate })}>
                    <Text style={style.liTxt}>Teknik Karate</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Sumpah Karate", data: data.sumpahKarate })}>
                    <Text style={style.liTxt}>Sumpah Karate</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Tingkatan Sabuk", data: data.sabukKarate })}>
                    <Text style={style.liTxt}>Tingkatan Sabuk</Text>
                </TouchableOpacity>

            </Container >
        )
    }
}

export default Tentang

const style = {
    li: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom: 15,
        marginLeft: 50,
        marginRight: 50,
        borderWidth: 3,
    },
    liTxt: {
        fontSize: 22,
        fontWeight: 'bold',
    }
}

const data = {
    filosofi: `
        <h3>
            Filosofi Karate
        </h3>
        <p>
            Bagi praktisi Karate harus memahami 20 Filosofi Karate Gichin Funakoshi,
            yaitu:
        </p>
        <p>
            1. Dalam karate mulai dengan sebuah hormat ( rei ) dan diakhiri dengan
            sebuah siikap hormat.
        </p>
        <p>
            2. Tidak ada sikap menyerang lebih dahulu dalam karate.
        </p>
        <p>
            3. Karate adalah sebuah pertolongan kepada keadilan.
        </p>
        <p>
            4. Pertama kenali dirimu sendiri baru orang lain.
        </p>
        <p>
            5. Pertama semangat, kedua teknik.
        </p>
        <p>
            6. Bersiaplah untuk membebaskan pikiranmu.
        </p>
        <p>
            7. Kecelakaan muncul dari kekurangan perhatian.
        </p>
        <p>
            8. Berlatih karate tidak hanya didalam Dojo.
        </p>
        <p>
            9. Akan menghabiskan seluruh hidupmu untuk belajar Karate.
        </p>
        <p>
            10. Atasi masalahmu dengan semangat Karate.
        </p>
        <p>
            11. Karate sama dengan air panas. Jika tidak kau berikan panas yang tetap
            maka air itu akan dingin kembali.
        </p>
        <p>
            12. Jangan berpikir masalah menang dalam pertarungan tapi pikirkan
            bagaimana agar kau tidak kalah dalam pertarungan.
        </p>
        <p>
            13. Rahasia pertarungan tersembunyi dalam seni yang mengarahkannya.
        </p>
        <p>
            14. Bergeraklah mengikuti ( sesuai dengan ) lawanmu.
        </p>
        <p>
            15. Pikirkan bahwa kedua tangan dan kakimu adalah pedang.
        </p>
        <p>
            16. Segera setelah kau tinggalkan rumah untuk bkerja pikirkan bahwa jutaan
            lawan tengah menunggumu.
        </p>
        <p>
            17. Pemula pertama-tama harus menguasai kuda-kuda dan sikap badan rendah,
            posisi badan yang alamiah/wajar untuk tingkat lanjut.
        </p>
        <p>
            18. Berlatih Kata adalah satu hal dan menghadapi sebuah pertarungan nyata
            adalah hal lain lagi.
        </p>
        <p>
            19. Jangan lupa : aplikasi ringan dan berat dari kekuatan, merenggangkan
            dan mengerutkan badan, cepat dan lambat dari teknik.
        </p>
        <p>
            20. Carilah cara dan senantiasa berlatih sepanjang waktu
        </p>
    `,
    sumpahKarate: `
        <h3>
            Sumpah Karate
        </h3>
        <p>
            1. Sanggup memelihara kepribadian.
        </p>
        <p>
            2. Sanggup patuh pada kejujuran.
        </p>
        <p>
            3. Sanggup mempertinggi prestasi.
        </p>
        <p>
            4. Sanggup menjaga sopan santun.
        </p>
        <p>
            5. Sanggup menguasai diri.
        </p> 
    `,
    sejarahKarate: `
        <h2>
            Sejarah Karate di Dunia
        </h2>
        <p>
            Seni bela diri karate dibawa masuk ke Jepang lewat Okinawa. Seni bela diri
            ini pertama kali disebut “Tote” yang berarti seperti “Tangan China”. Waktu
            karate masuk ke Jepang, nasionalisme Jepang pada saat itu sedang
            tinggi-tingginya, sehingga Sensei Gichin Funakoshi mengubah kanji Okinawa
            (Tote: Tangan China) dalam kanji Jepang menjadi ‘karate’ (Tangan Kosong)
            agar lebih mudah diterima oleh masyarakat Jepang. (Pada saat itu, Okinawa
            belum menjadi bagian Jepang).
        </p>
        <p>
            Pada tahun 1923, Gichin Funakoshi untuk pertama kalinya memperagakan Te
            atau Okinawa-Te ini di Jepang. Berturut-turut kemudian pada tahun 1929
            tokoh-tokoh seperti Kenwa Mabuni, Choyun Miyagi berdatangan dari Okinawa
            dan menyebarkan karate di Jepang. Kenwa Mabuni menamakan alirannya
            Shitoryu, Choyun Miyagi menamakan alirannya Gojuryu, dan Gichin Funakoshi
            menamakan alirannya Shotokan. Masutatsu Oyama kemudian secara resmi juga
            mendirikan aliran Karate baru yang dinamakan Kyokushin pada tahun 1956.
        </p>
        <p>
            Okinawa Te ini yang telah dipengaruhi oleh teknik-teknik seni bela diri
            dari Cina, sekali lagi berbaur dengan seni bela diri yang sudah ada di
            Jepang, sehingga mengalami perubahan-perubahan dan berkembang menjadi
            Karate seperti sekarang ini. Berkat upaya keras dari para tokoh ahli seni
            bela diri ini selama periode setelah Perang Dunia II, Karate kini telah
            berkembang pesat ke seluruh dun ia dan menjadi olah raga seni bela diri
            paling populer di seluruh dunia.
        </p>
        <p>
            Di negara Jepang, organisasi yang mewadahi olahraga karate seluruh Jepang
            adalah JKF. Adapun organisasi yang mewadahi karate seluruh dunia adalah WKF
            (dulu dikenal dengan nama WUKO – World Union of Karatedo Organizations).
            Ada pula ITKF (International Traditional Karate Federation) yang mewadahi
            karate tradisional. Adapun fungsi dari JKF dan WKF adalah terutama untuk
            meneguhkan karate yang bersifat “tanpa kontak langsung”, berbeda dengan
            aliran Kyokushin atau Daidojuku yang “kontak langsung”.
        </p>
        <h2>
            Sejarah Karate di Indonesia
        </h2>
        <p>
            Karate masuk di Indonesia bukan dibawa oleh tentara Jepang melainkan oleh
            mahasiswa-mahasiswa Indonesia yang kembali ke tanah air setelah
            menyelesaikan pendidikan mereka di Jepang. Pada tahun 1963, beberapa
            mahasiswa Indonesia, antara lain: Baud AD Adikusumo (seorang karateka yang
            mendapatkan sabuk hitam dari M. Nakayama, JKA Shotokan), Karianto
            Djojonegoro, Mochtar Ruskan dan Ottoman Noh, mendirikan Dojo di Jakarta.
            Mereka inilah yang mula-mula memperkenalkan karate (aliran Shoto-kan) di
            Indonesia, dan selanjutnya mereka membentuk wadah yang mereka namakan
            Persatuan Olahraga Karate Indonesia (PORKI) yang diresmikan tanggal 10
            Maret 1964 di Jakarta. Baud AD Adikusumo kemudian tercatat sebagai pelopor
            seni beladiri Karate di Indonesia dan juga pendiri Indonesia Karate-DO
            (INKADO).
        </p>
        <p>
            Setelah beliau, tercatat nama putra-putra bangsa Indonesia yang ikut
            berjasa mengembangkan berbagai aliran Karate di Indonesia, antara lain:
            Sabeth Mukhsin dari aliran Shotokan, pendiri Institut Karate-Do Indonesia
            (INKAI) dan Federasi Karate Tradisional Indonesia (FKTI), dan juga adalah
            Anton Lesiangi (sama-sama dari aliran Shotokan), pendiri Lembaga Karate-Do
            Indonesia/LEMKARI.
        </p>
        <p>
            Aliran Shotokan adalah yang paling populer di Indonesia. Selain Shotokan,
            Indonesia juga memiliki perguruan-perguruan dari aliran lain yaitu
            Wadodibawah asuhan Wado-ryu Karate-Do Indonesia (WADOKAI) yang didirikan
            oleh C.A. Taman dan Kushin-ryu Matsuzaki Karate-Do Indonesia (KKI) yang
            didirikan oleh Matsuzaki Horyu. Selain itu juga dikenal Setyo Haryono dan
            beberapa tokoh lainnya membawa aliran Goju-ryu, dan Nardi T. Nirwanto
            dengan beberapa tokoh lainnya membawa aliran Kyokushin. Aliran Shito-ryu
            juga tumbuh di Indonesia dibawah perguruan GABDIKA Shitoryu (dengan
            tokohnya Dr. Markus Basuki) dan SHINDOKA (dengan tokohnya Bert Lengkong).
            Selain aliran-aliran yang bersumber dari Jepang di atas, ada juga beberapa
            aliran Karate di Indonesia yang dikembangkan oleh putra-putra bangsa
            Indonesia sendiri, sehingga menjadi independen dan tidak terikat dengan
            aturan dari Hombu Dojo (Dojo Pusat) di negeri Jepang.
        </p>
        <p>
            Disamping ex-mahasiswa-mahasiswa tersebut di atas, orang-orang Jepang yang
            datang ke Indonesia dalam rangka usaha telah pula ikut memberikan warna
            bagi perkembangan karate di Indonesia. Mereka-mereka ini antara lain:
            Matsusaki (Kushinryu-1966), Ishi (Gojuryu-1969), Hayashi (Shitoryu-1971)
            dan Oyama (Kyokushinkai-1967).
        </p>
        <p>
            Karate ternyata memperoleh banyak penggemar, yang implementasinya terlihat
            muncul dari berbagai macam organisasi (Pengurus) karate, dengan berbagai
            aliran seperti yang dianut oleh masing-masing pendiri perguruan. Banyaknya
            perguruan karate dengan berbagai aliran menyebabkan terjadinya ketidak
            cocokan diantara para tokoh tersebut, sehingga menimbulkan perpecahan di
            dalam tubuh PORKI. Namun akhirnya dengan adanya kesepakatan dari para
            tokoh-tokoh karate untuk kembali bersatu dalam upaya mengembangkan karate
            di tanah air sehingga pada tahun 1972 hasil Kongres ke IV PORKI,
            terbentuklah satu wadah organisasi karate yang diberi nama Federasi
            Olahraga Karate-Do Indonesia (FORKI).
        </p>
        <p>
            Sejak FORKI berdiri sampai dengan saat ini kepengurusan di tingkat Pusat
            yang dikenal dengan nama Pengurus Besar/PB, organisasi ini telah dipimpin
            oleh 6 orang Ketua Umum dan periodisasi kepengurusannyapun mengalami 3 kali
            perobahan masa periodisasi yaitu: periode 5 tahun (ditetapkan pada Kongres
            tahun 1972 untuk kepengurusan periode tahun 1972 – 1977), periodisasi 3
            tahun (ditetapkan pada kongres tahun 1997 untuk kepengurusan periode tahun
            1997 – 1980), dan periodisasi 4 tahun (Berlaku sejak kongres tahun 1980
            sampai sekarang)
        </p>
    `,
    teknikKarate: `
        <p>
            <strong>Kata </strong>
        </p>
        <p>
            Kata adalah gabungan atau perpaduan dari rangkaian gerak dasar pukulan,
            tangkisan, dan tendangan menjadi satu kesatuan bentuk yang nyata (Sujoto
            J.B, 1996 : 137).
        </p>
        <p>
            Dalam Kata tersimpan bentuk-bentuk sikap dalam karate yang wajib dimiliki,
            seperti kontrol (diri), tenaga (power), kecepatan, juga bentuk penghayatan
            karate dalam realitas sebenarnya (Phang Victorianus, 2012 : 45).
        </p>
        <p>
            Kata memainkan peranan yang penting dalam latihan karate. Setiap kata
            memiliki embusen (pola dan arah) dan bunkai (praktik) yang berbeda-beda
            tergantung dari kata yang sedang dikerjakan. Kata dalam karate memiliki
            makna dan arti yang berbeda.
        </p>
        <p>
            <strong>Kihon </strong>
        </p>
        <p>
            Menurut Sujoto J.B (1996:53) kihon berarti pondasi / awal / akar dalam
            bahasa Jepang. Dari sudut pandang diartikan sebagai unsur terkecil yang
            menjadi dasar pembentuk sebuah teknik yang biasanya berupa rangkaian dari
            beberapa buah teknik besar. Dalam Pencak Silat mungkin kihon bisa dianggap
            sama dengan jurus tunggal, Sedangkan dalam Karate sendiri kihon lebih
            berarti sebagai bentuk – bentuk baku yang menjadi acuan dasar gerakan dari
            semua teknik atau gerakan yang mungkin dilakukan dalam jurus (Kata) maupun
            pertarungan (Kumite) .
        </p>
        <p>
            Kihon dalam karate haruslah bermula dari pinggul pada saat akan memulai
            sebuah kihon apapun seluruh anggota tubuh haruslah dalam posisi dan kondisi
            Shizentai tanpa ketegangan sedikit pun juga. Bersamaan dengan memulai
            gerakan harus dilakukan pengambilan nafas lewat hidung yang kemudian
            dimampatkan secara terfokus ke arah dengan jalan pengerasan daerah perut
            bagian bawah secara cepat dan pada saat gerakan sudah sempurna bentuk dan
            arahnya nafas dikeluarkan lewat mulut sambil mengeraskan anggota tubuh yang
            berkaitan dengan bentuk kihon yang dilakukan.
        </p>
        <p>
            <strong>Kumite </strong>
        </p>
        <p>
            Kumite secara harfiah berarti “pertemuan tangan”. Kumite dilakukan oleh
            murid-murid tingkat lanjut (sabuk biru atau lebih). Sebelum melakukan
            kumite bebas (jiyu Kumite) praktisi mempelajari kumite yang diatur (go hon
            kumite). Untuk kumite aliran olahraga, lebih dikenal dengan Kumite
            Pertandingan atau Kumite Shiai.
        </p>
        <p>
            Kumite adalah suatu metode latihan yang menggunakan teknik serangan dan
            teknik bertahan di dalam kata diaplikasikan melalui pertarungan dengan
            lawan yang saling berhadapan (Prihastono Arief, 1995 : 46). Menurut Sujoto
            J.B (1996 : 152), kumite adalah suatu metode latihan – latihan teknik dasar
            pukulan, tangkisan, dan tendangan. Dari kedua pendapat tersebut di atas
            dapat diartikan bahwa kumite merupakan suatu metode latihan yang bertujuan
            untuk melatih teknik-teknik karate baik teknik menyerang dan teknik
            bertahan yang dilakukan secara berpasangan.
        </p>
    `,
    sabukKarate: `
        <p>
            <strong>Sabuk Biru</strong>
        </p>
        <p>
            Warna biru pada sabuk karate ini menggambarkan samudra dan langit.
            Pengambilan samudra dan langit sebagai makna warna sabuk ini diharpkan
            seorang karatekan memiliki semangat luas seperti angkasa dan sedalam
            samudra.
        </p>
        <p>
            Karatekan sudah harus berani menghadapi segala tantangan dan rintangan
            untuk mengeksplor lagi skill yang dimiliki dan harus mampu menganggap bahwa
            latihan berat yang dilakukan merupakan kegiatan yang menyenagkan untuk
            mendapatkan manfaat yang banyak. Dalam tingkatan ini, karatekan juga harus
            mampu mengontrol emosi dan disiplin dalam segala aspek kehidupannya.
        </p>
        <p>
            <strong>Sabuk Coklat</strong>
        </p>
        <p>
            Tingkatan sabuk karate yang ke-5 adalah sabuk coklat atau chaobi. Warna
            coklat pada warna sabuk ini melambangkan warna tanah, dimana tanah memiliki
            sifat stabil dan berbobot.
        </p>
        <p>
            Seorang karateka yang sudah berda pada tingkatan ini, dari tingkatan kyu 2
            sampai kyu 1 harus mampu memberikan kestabilan sikap dibanding dengan
            pemegang sabuk yang ada di bawahnya, serta harus mampu melindungi
            junior-juniornya.
        </p>
        <p>
            Karakter tanah yang dekat dengan bumi juga mencerminkan sikap menjejah bumi
            (down to earth) dan rendah hati kepada sesama.
        </p>
        <p>
            <strong>Sabuk Hijau</strong>
        </p>
        <p>
            Tingkat sabuk hijau merupakan tinkatan karate yang sudah mempelajari
            tehnik-tehnik yang rumit dan membutuhkan konsentrasi tinggi. Warna hijau
            pada sabuk karate mepresentasikan warna rumput dan pepohonan.
        </p>
        <p>
            Pemegang sabuk hijau harus sudah mampu memahami dan menggal lebih dalam
            segala sesuatu yang berkaitan dengan karate, seiring dengan bertambahnya
            semangat dan tehnik karate yang sudah dikuasai.
        </p>
        <p>
            Sifat warna hijau pada sabuk ini menggambarkan pertumbuhan dan harmoni,
            dimana seorang karatekan dituntut untuk bisa memberikan harmoni dan
            keseimbangan bagi lingkungan
        </p>
        <p>
            <strong>Sabuk Hitam</strong>
        </p>
        <p>
            Warna hitam yang menjadi warna untuk tingkatan sabuk karate tertinggi
            melambangkan keteguhan dan sikap percaya diri didasari oleh nilai kebaikan
            yang universal.
        </p>
        <p>
            Tingkatan sabuk ini sangat diidam-idamkan oleh para karatekan. Namun,
            dibalik semua kewibawahan dan prestasi pemegang sabuk ini, ada segudang
            tanggung jawab yang harus diemban.
        </p>
        <p>
            <strong>Sabuk Kuning</strong>
        </p>
        <p>
            Sabuk kuning merupakan tingkatan kedua dalam beladiri karate. Dalam
            tingkatan sabuk karate, warna kuning melambangkan warna matahari. Warna
            matahari yang kuning dan cerah diibaratkan seorang karateka pemain karate
            sedang melihat “hari biru” yang cerah, dimana dia telah memahami semangat
            karate yang berkembang dalam kepribadian dan juga teknik yang telah
            dipelajari.
        </p>
        <p>
            Sabuk kuning juga merupakan tingkatan sabuk pemula terakhir, dimana siswa
            yang masih dalam tingkatan sabuk karate pemula masih memepelajari
            gerakan-gerakan dasar. Siswa yang telah melewati tingktatan sabuk karate
            ini selanjutnya akan mempelajari tehnik-tehnik karate yang lebih rumit,
            bahkan sudah diizinkan untuk mengikuti turnamen tingkat dasar.
        </p>
        <p>
            <strong>Sabuk Putih</strong>
        </p>
        <p>
            Sabuk putih merupakan tingkatan paling dasar dari bela diri karate. Wara
            putih melambangkan kesucian dan kemurnian. Kesucian dan kemurnian yang
            dimasud disini adalah kondisi dasar dari siswa krate untuk menerima dan
            mengolah hasil latihan dari para guru atau senpai.
        </p>
        <p>
            Maksudnya adalah, perkembangan siswa karate tergantung dari materi yang
            telah diberikan oleh senpai. Setelah senpai memerikan materi kepada siswa
            sesuai dengan standar, perkembangan kemampuan berada di individu
            masing-masing, tinggal seberapa serius siswa berlatih dan menguasai
            pelajaran yang diberikan.
        </p>
    `,
    istilahKarate: `
        <p>
            <strong>Arena Berlatih dan Seragam</strong>
        </p>
        <p>
            a) Dojo : tempat berlangsungnya latihan (berupa tempat khusus yang hanya
            digunakan untuk latihan).
        </p>
        <p>
            b) Gi / Dogi / Karate-Gi : baju karateka.
        </p>
        <p>
            c) Obi : sabuk karate.
        </p>
        <p>
            d) KYU : tingkatan sabuk dasar (berwarna), mulai KYU 10 hingga KYU 1.
        </p>
        <p>
            e) DAN : tingakatan sabuk senior atau mahir (hitam), mulai DAN 1 hingga DAN
            10.
        </p>
        <p>
            <strong>Istilah KATA</strong>
        </p>
        <p>
            Kata adalah berupa jurus yang berasal dari beberapa gerakan dasar atau
            kihon.
        </p>
        <div align="center">
            <table border="1" cellspacing="0" cellpadding="0" width="0">
                <tbody>
                    <tr>
                        <td width="27%">
                            <p>
                                Kata
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Arti
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Heian Shodan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Pikiran yang damai (1)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Heian Nidan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Pikiran yang damai (2)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Heian Sandan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Pikiran yang damai (3)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Heian Yondan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Pikiran yang damai (4)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Heian Godan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Pikiran yang damai (5)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Tekki Shodan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Satria yang kuat, kuda-kuda yang kuat (1)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Tekki Nidan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Satria yang kuat, kuda-kuda yang kuat (2)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Tekki Sandan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Satria yang kuat, kuda-kuda yang kuat (3)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Bassai Dai
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Menembus benteng (besar)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Kanku Dai
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Menatap langit (besar)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Enpi
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Burung layang-layang terbang
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Hangetsu
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Bulan separuh
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Jion
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Nama biksu Budha, pengampunan
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Nijushiho
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                24 langkah
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Sochin
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Memberi kedamaian bagi orang banyak
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Bassai Sho
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Menembus benteng (kecil)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Kanku Sho
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Menatap langit (kecil)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Jitte
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Bertarung seolah-olah dengan kekuatan 10 orang
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Chinte
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Tangan yang luar biasa
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Meikyo
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Cermin jiwa
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Jiin
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Gema Kuil, Dasar kuil
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Gankaku
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Bangau diatas batu
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Wankan
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Mahkota raja
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Gojushiho Sho
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                54 langkah (kecil)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Gojushiho Dai
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                54 langkah (besar)
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="27%">
                            <p>
                                Unsu
                            </p>
                        </td>
                        <td width="72%">
                            <p>
                                Tangan seperti (menyibak) awan di angkasa
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p>
            <strong>Istilah Kuda – kuda (Dachi)</strong>
        </p>
        <p>
            a) Hachiji-Dachi : Kuda-kuda Dasar ( Kaki Dibuka selebar bahu ).
        </p>
        <p>
            b) Zen-Kutsu-Dachi : Kuda-kuda berat depan.
        </p>
        <p>
            c) Ko-Kutsu-Dachi : Kuda-kuda berat belakang.
        </p>
        <p>
            d) Hangetsu-Dachi : Kuda-kuda berat tengah ( dalam Kata Hangetsu ).
        </p>
        <p>
            e) Heisoku-Dachi : Kuda-kuda berat tengah tatapi kedua kaki rapat ( dalam
            Kata Unsu ).
        </p>
        <p>
            f) Neko-Ashi-Dachi : Kuda-kuda berat belakang ( dalam Kata Unsu ).
        </p>
        <p>
            g) Sanshin-Dachi : Kuda-kuda berat tengah
        </p>
        <p>
            h) Sochin-Dachi : Kuda-kuda berat tengah ( dalam Kata Sochin )
        </p>
        <p>
            <strong>Istilah Kumite (Bertarung)</strong>
        </p>
        <p>
            a) KIHON IPPON KUMITE : Pertarungan Dasar Satu Langkah.
        </p>
        <p>
            b) GO-HON KUMITE : Pertarungan Lima Langkah.
        </p>
        <p>
            c) SANBON KUMITE : Pertarungan Tiga Langkah.
        </p>
        <p>
            d) KEASHI IPPON KUMITE : Pertarungan Dua Langkah.
        </p>
        <p>
            e) JIYU IPPON KUMITE : Pertarungan Semi Bebas.
        </p>
        <p>
            f) OKURI JIYU IPPON KUMITE : Pertarungan Semi Bebas Dua Langkah.
        </p>
        <p>
            g) JIYU KUMITE : Pertarungan Gaya Bebas.
        </p>
        <p>
            <strong>Istilah Pertandingan</strong>
        </p>
        <p>
            a) Kumite : perkelahian atau bertarung.
        </p>
        <p>
            b) Ippon kumite : perkelahian 1 langkah.
        </p>
        <p>
            c) Sanbon kumite : perkelahian 3 langkah.
        </p>
        <p>
            d) Jiyo kumite : perkelahian atau bertarung 1 lawan 1 secara terhormat.
        </p>
        <p>
            <strong>Istilah Pukulan (Zuki)</strong>
        </p>
        <p>
            a) Oi-Zuki-Chudan : Pukulan ke arah Perut atau ulu hati.
        </p>
        <p>
            b) Oi-Zuki-Jodan : Pukulan ke arah kepala.
        </p>
        <p>
            c) Kisame-Zuki : Pukulan ke arah kepala tetapi kaki tidak melangkah.
        </p>
        <p>
            d) Gyaku-Zuki : Pukulan ke arah perut tetapi kaki tidak melangkah.
        </p>
        <p>
            e) Ura-Zuki : Pukulan yang bentuknya seperti Soto-Ude-Uke.
        </p>
        <p>
            f) Morote-Zuki : Pukulan dan dorongan.
        </p>
        <p>
            g) Agi-Zuki : Pukulan dengan tangan bagian dalam dan bentuknya seperti
            Agi-Uke.
        </p>
        <p>
            h) Choku-Zuki : Pukulan kearah perut dengan Kuda-kudaHachiji-Dachi.
        </p>
        <p>
            i) Kage-Zuki : Pukulan kesamping exs pada Kata Tekki Shodan.
        </p>
        <p>
            j) Tate-Zuki : Pukulan yang bentuknya seperti Uchi-Ude-Uke.
        </p>
        <p>
            k) Yama-Zuki : Pukulan menggunung / Pukulan ganda dengan kedua tangan.
        </p>
        <p>
            l) Morote-Hisame-Zuki : Pukulan dengan kedua tangan.
        </p>
        <p>
            m) Tetsui-Uchi : Tangan palu.
        </p>
        <p>
            n) Uraken-Uchi : Pukulan menyamping.
        </p>
        <p>
            o) Haishu-Uchi : Tangan pedang.
        </p>
        <p>
            p) Haito-Uchi : Tangan pedang.
        </p>
        <p>
            q) Empi : Sikutan.
        </p>
        <p>
            r) Shuto-Uchi : Tangan pedang.
        </p>
        <p>
            s) Tate-Shuto : Tangan pedang.
        </p>
        <p>
            <strong>Istilah Tangkisan (Uke)</strong>
        </p>
        <p>
            a) Gedan Barai : Tangkisan bawah atau tangkisan Mae-Geri.
        </p>
        <p>
            b) Soto-Ude-Uke : Tangkisan tengah yang datangnya dari belakang telinga.
        </p>
        <p>
            c) Uchi-Ude-Uke : Tangkisan tengah yang datangnya dari bawah ketiak.
        </p>
        <p>
            d) Agi-Uke : Tangkisan atas.
        </p>
        <p>
            e) Shuto-Uke : Tangkisan tangan pedang.
        </p>
        <p>
            f) Juji-Uke : Tangkisan dengan kedua tangan disilang.
        </p>
        <p>
            Morote-Uke : Tangkisan yang bentuknya seperti Morote-Zuki.
        </p>
        <p>
            <strong>Istilah Tendangan (Geri)</strong>
        </p>
        <p>
            a) Mae-Geri: Tendangan ke arah Perut atau Kepala dengan arah ke depan.
        </p>
        <p>
            b) Mawashi-Geri: Tendangan dengan Kaki bagian atas.
        </p>
        <p>
            c) Yoko-Geri-Kekome: Tendangan dengan Kaki bagian samping ( di sodok ).
        </p>
        <p>
            d) Yoko-Geri-Keange: Tendangan dengan Kaki bagian samping ( di snap ).
        </p>
        <p>
            e) Usiro-Geri: Tendangan ke belakang.
        </p>
        <p>
            f) Tate-Shuto : Tangan pedang.
        </p>
        <p>
            <strong>Menghormat</strong>
        </p>
        <p>
            Beberapa istilah penghormatan dalam karate, diantaranya adalah:
        </p>
        <p>
            a) Mokuso : aba-aba untuk menutup mata dan memulai meditasi.
        </p>
        <p>
            b) Mokuso yame : aba-aba untuk membuka mata setelah selesai meditasi.
        </p>
        <p>
            c) Shomen ni rei : menghormat kedepan (pada pertandingan).
        </p>
        <p>
            d) Shihan ni rei : menghormat kepada shihan.
        </p>
        <p>
            e) Sensei/senpai ni rei : menghormat kepada pembina.
        </p>
        <p>
            f) Otagai ni rei : menghormat ke lawan (orang lain).
        </p>
        <p>
            <strong>Sapaan Menurut Tingkatan</strong>
        </p>
        <p>
            a) Sosai : presiden atau ketua umum dalam struktur organisasi perguruan.
        </p>
        <p>
            b) Kancho : kepala atau director dalam struktur organisasi perguruan.
        </p>
        <p>
            c) Shihan : master atau faunder (orang tertinggi diperguruan).
        </p>
        <p>
            d) Sensai : pembina perguruan (biasanya telah memiliki tingkatan DAN 3 ke
            atas).
        </p>
        <p>
            e) Yudansha : karateka dengan sabuk hitam hitam.
        </p>
        <p>
            f) Senpai : karateka dengan tingkatan lebih tinggi dari kita.
        </p>
        <p>
            g) Kohai : karateka dengan tingkatan sama.
        </p>
        <p>
            h) Dohai : karateka dengan tingkatan lebih rendah.
        </p>
        <p>
            i) Otagai : warga lain (lawan bertanding).
        </p>
    `,
}