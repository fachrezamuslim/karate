import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native'
import Container from '../component/container';
import { Actions } from 'react-native-router-flux';

class Kihon extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Container breadcumb judul="Teknik Kihon">
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Kihon - Kuda-kuda", data: data.kudakuda })}>
                    <Text style={style.liTxt}>Kuda-kuda</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Kihon - Pukulan", data: data.pukulan })}>
                    <Text style={style.liTxt}>Pukulan</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Kihon - Tendangan", data: data.tendangan })}>
                    <Text style={style.liTxt}>Tendangan</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Kihon - Tangkisan", data: data.tangkisan })}>
                    <Text style={style.liTxt}>Tangkisan</Text>
                </TouchableOpacity>
            </Container >
        )
    }
}

export default Kihon

const style = {
    li: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom: 15,
        marginLeft: 50,
        marginRight: 50,
        borderWidth: 3,
    },
    liTxt: {
        fontSize: 22,
        fontWeight: 'bold',
    }
}

const data = {
    kudakuda: `
        <div>
            <p>1. Zenkusudachi : adalah kuda-kuda dengan keadaan berat ke arah depan dan juga kedua posisi kaki tidak sejajar.</p>
            
        </div>
        <div>
            <p>2. Nekoashi dachi: kuda-kuda dengan kaki depan seperti orang jinjit.</p>
            
        </div>
        <div>
            <p>3. Kosa dachi : Kosa dachi yaitu Posisi (kuda-kuda) menyeberang</p>
            
        </div>
        <div>
            <p>4. Musubi dachi : kuda-kuda dengan kedua kaki tegap dan masing-masing jari kaki menyerang </p>
            
        </div>
        <div>
            <p>5. Kibadachi : Kiba dachi yaitu Posisi (kuda-kuda) menunggang kuda</p>
            
        </div>
        <div>
            <p>6. Kokutsudachi : adalah kuda-kuda dengan keadaan berat ke arah belakang dan juga kedua posisi kaki sejajar atau dalam satu garis.</p>
            
        </div>
        <div>
            <p>7. Heiko dachi : Sikap berdiri bebas dengan jari-jari kaki menghadap depan.</p>
            
        </div>
    `,
    pukulan: `
        <div>
            <p>1. Oizuki chudan : yaitu Pukulan menerjang/menghujam dengan target mengarah uluhati atau dada dan dimana kaki yang melangkah maka tangan itu juga yang melakukan pukulan.</p>
            
        </div>
        <div>
            <p>2. Gyakuzuki : yaitu Pukulan menerjang/menghujam dengan target mengarah uluhati atau dada dan dimana kaki yang melangkah maka tangan yang sebaliknya yang melakukan pukulan.</p>
            
        </div>
        <div>
            <p>3. Heiko zuki : pukulan dengan menggunkan kedua tangan sekaligus dalam satu kali gerakan dengan arah sasaran bagian dada.</p>
            
        </div>
        <div>
            <p>4. kizami zuki : yaitu Pukulan pertama dengan tinju (tangan) yang di depan</p>
            
        </div>
        <div>
            <p>5. nukite : yaitu Pukulan dengan jari lurus kecuali ibu jari (tangan terbuka)</p>
            
        </div>
        <div>
            <p>6. Yama zuki : pukulan dengan menggunkan kedua tangan sekaligus dalam satu kali gerakan dengan arah sasaran bagian kepala dan badan.</p>
            
        </div>
        <div>
            <p>7. Tettsui uchi : pukulan dengan gaya seperti menumbuk atau hammer hand</p>
            
        </div>
    `,
    tendangan: `
        <div>
            <p>1.	maegeri chudan : yaitu Tendangan menempel (menyodok) kaki depan dengan target daerah perut.</p>
            
        </div>
        <div>
            <p>2.	maegeri jodan : yaitu Tendangan menempel (menyodok) kaki depan dengan target daerah leher atau wajah.</p>
            
        </div>
        <div>
            <p>3.	yoko keage : yaitu Tendangan samping menempel dengan target daerah leher atau wajah.</p>
            
        </div>
        <div>
            <p>4.	Mawashigeri : yaitu Tendangan memutar seperti menampar wajah namun dengan punggung kaki </p>
            
        </div>
        <div>
            <p>5.	Ushirogeri : Ushiro geri Tendangan belakang dengan cara menghujam dengan target daerah belakang kepala</p>
            
        </div>
        <div>
            <p>6.	kin geri : Tendangan ganda; kombinasi tendangan</p>
            
        </div>
        <div>
            <p>7.	yoko kekomi : yaitu Tendangan samping menempel dengan target daerah sekitar perut</p>
            
        </div>
    `,
    tangkisan: `
        <div>
            <p>1.	ageuke : tangkisan menutupi daerah wajah atau kepala</p>
            
        </div>
        <div>
            <p>2.	sotouke / udeuke : tangkisan kearah dalam mengantisipasi daerah uluhati atau dada</p>
            
        </div>
        <div>
            <p>3.	uchiuke : tangkisan kearah luar mengantisipasi daerah uluhati atau dada</p>
            
        </div>
        <div>
            <p>4.	shutouke : yaitu Tangkisan pisau tangan dengan mengantisipasi serangan ke arah uluhati atau dada</p>
            
        </div>
        <div>
            <p>5.	haiwan uke : tangkisan menggunakan kedua tangan dalam satu gerakan dengan mengantisipasi serangan ke arah kepala atau wajah</p>
            
        </div>
        <div>
            <p>6.	empi : tangkisan samping dengan menggunakan sikut</p>
            
        </div>
        <div>
            <p>7.	gedan barai : tangkisan ke arah bawah mengantisipasi serangan ke bagian daerah kemaluan atau alat vital.</p>
            
        </div>
    `,
}