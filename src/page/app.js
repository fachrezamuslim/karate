import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

// Page
import ScreenSplash from './screensplash';
import Home from './home';
import Tentang from '../page/tentang';
import Latihan from '../page/latihan';
import Read from '../page/read';
import Teknik from '../page/teknik';
import Kihon from '../page/kihon';

class App extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene
                        key="screensplash"
                        hideNavBar
                        component={ScreenSplash}
                    />
                    <Scene
                        key="home"
                        hideNavBar
                        component={Home}
                    />
                    <Scene
                        key="tentang"
                        hideNavBar
                        component={Tentang}
                    />
                    <Scene
                        key="latihan"
                        hideNavBar
                        component={Latihan}
                    />
                    <Scene
                        key="teknik"
                        hideNavBar
                        component={Teknik}
                    // initial
                    />
                    <Scene
                        key="kihon"
                        hideNavBar
                        component={Kihon}
                    // initial
                    />
                    <Scene
                        key="read"
                        hideNavBar
                        component={Read}
                    />
                </Scene>
            </Router >
        )
    }
}

export default App