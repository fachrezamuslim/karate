import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

class ScreenSplash extends Component {
    constructor(props) {
        super(props)

    }

    componentDidMount() {
        setTimeout(() => {
            Actions.push('home');
        }, 3000);
    }

    render() {
        return (
            <View style={{ display: 'flex' }}>
                <Image
                    source={require('../karate/img/image-splash-screen.jpg')}
                    style={{
                        width: '100%',
                        height: '100%',
                        backgroundColor: 'red',
                    }}
                    resizeMode="stretch"
                />
            </View>
        )
    }
}

export default ScreenSplash