import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native'
import Container from '../component/container';
import { Actions } from 'react-native-router-flux';

class Latihan extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Container breadcumb judul="Latihan">
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Putih", data: data.putih })}>
                    <Text style={style.liTxt}>Putih</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Kuning", data: data.kuning })}>
                    <Text style={style.liTxt}>Kuning</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Hijau", data: data.hijau })}>
                    <Text style={style.liTxt}>Hijau</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Biru", data: data.biru })}>
                    <Text style={style.liTxt}>Biru</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.li} onPress={() => Actions.read({ judul: "Coklat", data: data.coklat })}>
                    <Text style={style.liTxt}>Coklat</Text>
                </TouchableOpacity>
            </Container >
        )
    }
}

export default Latihan

const style = {
    li: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "rgba(255,255,255,1)",
        marginBottom: 15,
        marginLeft: 50,
        marginRight: 50,
        borderWidth: 3,
    },
    liTxt: {
        fontSize: 22,
        fontWeight: 'bold',
    }
}

const data = {
    putih: `
        <h3 align="left">
            <a name="_Toc513211689">Materi Sabuk Putih</a>
        </h3>
        <p align="left">
            a) KIHON Di awali Gedan Barai kaki kiri depan
        </p>
        <p align="left">
            1. Maju oizuki chudan 5x
        </p>
        <p align="left">
            2. Mundur ageuke 5x
        </p>
        <p align="left">
            3. Maju sotouke 5x
        </p>
        <p align="left">
            4. Mundur uchiuke 5x
        </p>
        <p align="left">
            5. Maju shutouke 5x, Magate gedan barai – kamaite
        </p>
        <p align="left">
            6. Maju maegeri chudan 5x, Magate gedan barai - kamaite
        </p>
        <p align="left">
            7. Maju maegeri jodan 5x, Magate gedan barai Hadap team penguji kibadachi,
            kamaite kanan,
        </p>
        <p align="left">
            8. maju yokokeage 5x, kamaite kiri
        </p>
        <p align="left">
            9. Maju yoko keage 5x, Magate gedan barai – yame.
            <br/>
            <br/>
        </p>
        <p align="left">
            b) KATA
        </p>
        <p align="left">
            1. Heian Sodan
        </p>
        <p align="left">
            2. Heian Nidan
        </p>
        <p align="left">
            c) KUMITE :
        </p>
        <p align="left">
            1. Dari gedan barai, kaki kiri depan maju oizuki jodan 5x, lawan mundur age
            uke 5x bergantian, yame
        </p>
        <p align="left">
            2. Dari gedan barai, kaki kiri depan maju oizuki chudan 5x lawan mundur
            sotouke 5x, bergantian, YAME SELESAI.
        </p>
    `,
    kuning: `
        <h3 align="left">
            <a name="_Toc513211690">Materi </a>
            Sabuk Kuning
        </h3>
        <p align="left">
            a) KIHON Di awali Gedan Barai kaki kiri depan
        </p>
        <p align="left">
            1. Maju oizuki chudan 5x
        </p>
        <p align="left">
            2. Mundur ageuke 5x
        </p>
        <p align="left">
            3. Maju sotouke 5x
        </p>
        <p align="left">
            4. Mundur uchiuke 5x
        </p>
        <p align="left">
            5. Maju shutouke 5x, Magate gedan barai – kamaite
        </p>
        <p align="left">
            6. Maju maegeri chudan 5x, Magate gedan barai - kamaite
        </p>
        <p align="left">
            7. Maju maegeri jodan 5x, Magate gedan barai Hadap team penguji kibadachi,
            kamaite kanan,
        </p>
        <p align="left">
            8. maju yokokeage 5x, kamaite kiri
        </p>
        <p align="left">
            9. Maju yoko keage 5x, Magate gedan barai – yame.
            <br/>
            <br/>
        </p>
        <p align="left">
            b) KATA
        </p>
        <p align="left">
            1. Heian Nidan
        </p>
        <p align="left">
            2. Heian Sandan
            <br/>
            <br/>
        </p>
        <p align="left">
            c) KUMITE :
        </p>
        <p align="left">
            1. Dari gedan barai, kaki kiri depan maju oizuki jodan 5x lawan mundur age
            uke 5x, bergantian,
        </p>
        <p align="left">
            2. Dari gedan barai, kaki kiri depan maju oizuki chudan 5x, lawan mundur
            sotouke 5x, bergantian, YAME, SELESAI
        </p>
    `,
    hijau: `
        <h3 align="left">
            <a name="_Toc513211691">Materi Sabuk Hijau</a>
        </h3>
        <p align="left">
            a) KIHON Di awali Gedan Barai kaki kiri depan
        </p>
        <p align="left">
            1. Maju oizuki chudan 5x
        </p>
        <p align="left">
            2. Mundur ageuke gyakuzuki 5x
        </p>
        <p align="left">
            3. Maju sotouke gyakuzuki 5x
        </p>
        <p align="left">
            4. Mundur uchiuke gyakuzuki 5x
        </p>
        <p align="left">
            5. Maju shutouke 5x, Magate gedan barai – kamaite
        </p>
        <p align="left">
            6. Maju maegeri chudan 5x, Magate gedan barai - kamaite
        </p>
        <p align="left">
            7. Maju maegeri jodan 5x, Magate gedan barai menghadap team penguji kamaite
            kanan
        </p>
        <p align="left">
            8. Maju keange kaki kanan 5x, kamaite kiri
        </p>
        <p align="left">
            9. Maju keange kaki kiri 5x, kamite kanan
        </p>
        <p align="left">
            10. Maju yoko kekomi kaki kanan 5x, kamaite kiri
        </p>
        <p align="left">
            11. Maju yoko kekomi kaki kiri 5x, Magate gedan barai – YAME
            <br/>
            <br/>
        </p>
        <p align="left">
            b) KATA
        </p>
        <p align="left">
            1. Heian Sandan
        </p>
        <p align="left">
            2. Heian Yondan
            <br/>
            <br/>
        </p>
        <p align="left">
            c) KUMITE :
        </p>
        <p align="left">
            1. Kihon ippon kumite jodan, kaki kiri depan
        </p>
        <p align="left">
            2. Kihon ippon kumite chudan, kaki kiri depan
        </p>
        <p align="left">
            3. Kihon ippon kumite maegeri, kaki kiri depan
            <br/>
            Berikutnya kaki kanan depan bergantian, YAME SELESAI.
        </p>
    `,
    biru: `
        <h3 align="left">
            <a name="_Toc513211692">Materi </a>
            Sabuk Biru
        </h3>
        <p align="left">
            a) KIHON diawali gedan barai, kaki kiri depan
        </p>
        <p align="left">
            1. Maju oizuki chudan, 5x , Magate gedan barai,
        </p>
        <p align="left">
            2. Maju sambon renzuki 5x,
        </p>
        <p align="left">
            3. Mundur, ageuke gyakuzuki 5x,
        </p>
        <p align="left">
            4. Maju sotouke Yoko empi 5x,
        </p>
        <p align="left">
            5. Mundur uchiuke gyakuzuki 5x,
        </p>
        <p align="left">
            6. Maju shutouke nukite 5x, Magate gedan barai kamaite
        </p>
        <p align="left">
            7. Maju maegeri Jodan 5x, Magate gedan barai kamaite
        </p>
        <p align="left">
            8. Maju maegeri chudan 5x, Magate gedan barai kamaite
        </p>
        <p align="left">
            9. Maju mawashigeri 5 x, Magate gedan barai, hadap team penguji kibadachi
            kamaite kekanan
        </p>
        <p align="left">
            10. Maju yoko keage kaki kanan 5x, kamaite kekiri
        </p>
        <p align="left">
            11. Maju yoko keage kaki kiri 5 x, kamaite kekanan
        </p>
        <p align="left">
            12. Maju yoko kekomi kaki kanan 5x, kamaite kekiri,
        </p>
        <p align="left">
            13. Maju yoko kekomi kaki kiri 5 x, Magate gedan barai,
        </p>
        <p align="left">
            14. Gerak ditempat kaki rapat kamaite, maegeri, kekomi, ushirogeri kaki
            kanan dan kiri masing-masing 5x.
            <br/>
            <br/>
        </p>
        <p align="left">
            b) KATA
        </p>
        <p align="left">
            1. Heian Godan
        </p>
        <p align="left">
            2. Tekki sodan
            <br/>
            <br/>
        </p>
        <p align="left">
            c) Kumite
        </p>
        <p align="left">
            1. Kihon ippon kumite oizuki jodan, kanan dan kiri
        </p>
        <p align="left">
            2. Kihon Ippon kumite oizuki chudan, kanan dan kiri
        </p>
        <p align="left">
            3. Kihon ippon kumite maegeri, kanan dan kiri
        </p>
        <p align="left">
            4. Kihon ippon kumite yoko kekomi, kanan dan kiri,
        </p>
        <p align="left">
            5. Kihon ippon kumite mawashi geri, kanan dan kiri,
            <br/>
            YAME SELESAI
        </p>
    `,
    coklat: `
        <h3 align="left">
            <a name="_Toc513211693">Materi </a>
            Sabuk Coklat
        </h3>
        <p align="left">
            a) KIHON diawali gedan barai, kaki kiri depan
        </p>
        <p align="left">
            1. Maju oizuki chudan 5x, Magate gedan barai
        </p>
        <p align="left">
            2. Maju sambon renzuki 5x,
        </p>
        <p align="left">
            3. Mundur ageuke gyakuzuki 5x.,
        </p>
        <p align="left">
            4. Maju Sotouke Empi Uraken 5x.
        </p>
        <p align="left">
            5. Mundur uchiuke gyakuzuki 5x,
        </p>
        <p align="left">
            6. Maju shutouke nukite 5x Magate gedan barai kamaite
        </p>
        <p align="left">
            7. Maju maegeri jodan 5x. Magate gedan barai kamaite
        </p>
        <p align="left">
            8. Maju Maegeri chudan 5x, Magate gedan barai kamaite
        </p>
        <p align="left">
            9. Maju Ren geri, 5x, (chudan-jodan) 3x Magate gedan barai kamaite
        </p>
        <p align="left">
            10. Maju mawashigeri 5x Magate gedan barai hadap team penguji. Kibadachi
            Kamaite hadap kanan
        </p>
        <p align="left">
            11. Maju yoko keage kaki kanan, 5x Kamaite hadap kiri
        </p>
        <p align="left">
            12. maju yoko ke age kaki kiri, 5x Magate gidan barai
        </p>
        <p align="left">
            13. (Zenskusu dachi), maju Yoko kekomi, 5x Magate gidan barai
        </p>
        <p align="left">
            14. Gerak ditempat kaki rapat kamaite, maegeri, kekomi, ushirogeri, kaki
            kanan dan kaki kiri masing-masing 5x,
            <br/>
            <br/>
        </p>
        <p align="left">
            b) KATA
        </p>
        <p align="left">
            1. Tekki Sodan
        </p>
        <p align="left">
            2. Tokui Kata pilih satu dari: Bassai – Dai, Tekki Nidan, Tekki Sandan
            <br/>
            <br/>
        </p>
        <p align="left">
            c) KUMITE
        </p>
        <p>
            Jiyu Ippon kumite (perkelahian setengah bebas dengan perjanjian) dengan
            tehnik serangan : oizuki jodan, oizuki chudan, maegeri, yokokekomi, dan
            mawashigeri (kanan dan kiri bergantian) YAME SELESAI.
        </p>
    `,
}