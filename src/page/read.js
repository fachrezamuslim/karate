import React, { Component } from 'react';
import { Dimensions, ScrollView, Text } from 'react-native'
import Container from '../component/container';
import HTML from 'react-native-render-html';

class Read extends Component {
    constructor(props) {
        super(props)

    }

    render() {
        return (
            <Container breadcumb judul={this.props.judul}>

                <ScrollView style={{ flex: 1, marginTop: -15, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white' }}>
                    <HTML html={this.props.data} imagesMaxWidth={Dimensions.get('window').width} />
                </ScrollView>

            </Container >
        )
    }
}

export default Read